﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels
{
    public struct CellPosition
    {
        public int X;
        public int Y;

        public CellPosition(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static bool operator==(CellPosition cellLeft, CellPosition cellRight)
        {
            return cellLeft.Equals(cellRight);
        }

        public static bool operator !=(CellPosition cellLeft, CellPosition cellRight)
        {
            return !cellLeft.Equals(cellRight);
        }
    }
}
