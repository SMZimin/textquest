﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels
{
    internal class MapDto
    {
        public int SizeX { get; set; }
        public int SizeY { get; set; }
        public Cell[,] Cells { get; set; }
    }
}
