﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels
{
    public enum HeroEnum
    {
        Vova = 0,
        Serega = 1,
        Masha = 2,
        Sonya = 3,
        Guard = 4
    }
}
