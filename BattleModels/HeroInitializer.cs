﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels
{
    internal class HeroInitializer
    {
        public static List<Hero> Init(Map map)
        {
            var heroes = new List<Hero>();
            for (int i = 0; i < map.SizeX; i++)
            {
                for (int j = 0; j < map.SizeY; j++)
                {
                    Hero hero = TryGetHero(map[i, j]);
                    if (hero != null)
                        heroes.Add(hero);
                }
            }

            return heroes.OrderBy(h => h.HeroType).ToList();
        }

        private static Hero TryGetHero(Cell cell)
        {
            Hero hero = null;
            if (cell.State == CellStateEnum.Vova)
            {
                hero = new Hero(HeroEnum.Vova,
                    ConfigParams.HpVova,
                    ConfigParams.ArmorBasicVova,
                    ConfigParams.GrenadesVova,
                    ConfigParams.MedKitsBasicVova,
                    ConfigParams.AccuracyBasicVova,
                    cell,
                    HeroStrategy.MoveToFinish,
                    0,
                    ConfigParams.DamageVova);
            }
            else if (cell.State == CellStateEnum.Serega)
            {
                hero = new Hero(HeroEnum.Serega,
                    ConfigParams.HpSerega,
                    ConfigParams.ArmorSerega,
                    ConfigParams.GrenadesSerega,
                    ConfigParams.MedKitsSerega,
                    ConfigParams.AccuracySerega,
                    cell,
                    HeroStrategy.MoveToFinish,
                    ConfigParams.AggressionSerega,
                    ConfigParams.DamageSerega);
            }
            else if (cell.State == CellStateEnum.Masha)
            {
                hero = new Hero(HeroEnum.Masha,
                    ConfigParams.HpMasha,
                    ConfigParams.HpMasha,
                    ConfigParams.GrenadesMasha,
                    ConfigParams.MedKitsMasha,
                    ConfigParams.AccuracyMasha,
                    cell,
                    HeroStrategy.MoveToFinish,
                    ConfigParams.AggressionMasha,
                    ConfigParams.DamageMasha);
            }
            else if (cell.State == CellStateEnum.Sonya)
            {
                hero = new Hero(HeroEnum.Sonya,
                    ConfigParams.HpSonya,
                    ConfigParams.ArmorSonya,
                    ConfigParams.GrenadesSonya,
                    ConfigParams.MedKitsSonya,
                    ConfigParams.AccuracySonya,
                    cell,
                    HeroStrategy.MoveToFinish,
                    ConfigParams.AggressionSonya,
                    ConfigParams.DamageSonya);
            }
            else if (cell.State == CellStateEnum.Guard)
            {
                hero = new Hero(HeroEnum.Guard,
                    ConfigParams.HpGuard,
                    ConfigParams.ArmorGuard,
                    ConfigParams.GrenadesGuard,
                    ConfigParams.MedKitsGuard,
                    ConfigParams.AccuracyGuard,
                    cell,
                    HeroStrategy.MoveToNearestGoodGuy,
                    ConfigParams.AggressionGuard,
                    new Random().Next(2) * 10 + ConfigParams.DamageBasicGuard); // 30 или 40 или 50
            }

            return hero;
        }
    }
}
