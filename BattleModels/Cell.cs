﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels
{
    public class Cell
    {
        public CellPosition Position { get; }
        public CellStateEnum State { get; set; }
        public int? Distance { get; set; }

        public Cell(CellPosition position, CellStateEnum state)
        {
            Position = position;
            State = state;
            Distance = null;
        }
    }
}
