﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BattleControl.Events;
using BattleModels;

namespace BattleControl
{
    public partial class BattleGameControl : UserControl
    {
        private readonly string _pathToPicture = "..\\Picture\\karta-banka.png";
        private readonly string _pathToJson = "..\\Main\\map_walls_heroes_guards_new.json";
        private const int CellSizePx = 10;
        private const int HeroCircleRadius = 10;

        private Map Map { get; }
        private BattleEngine BattleEngine { get; }
        private bool WallsVisible { get; set; } = false;
        private bool GridVisible { get; set; } = false;

        public event EventHandler<BattleEndedEventArgs> BattleEnded;

        public BattleGameControl()
            :this(1, true, true)
        {
        }

        /// <summary>Создание контрола с игрой.</summary>
        /// <param name="medicineChestsCount">Количество аптечек.</param>
        /// <param name="hasArmor">Наличие бронежилета.</param>
        /// <param name="hasAim">Наличие прицела.</param>
        public BattleGameControl(int medicineChestsCount, bool hasArmor, bool hasAim)
        {
            InitializeComponent();
            Map = Map.FromJson(File.ReadAllText(_pathToJson));
            RedrawMap();
            BattleEngine = new BattleEngine(Map, 
                hasArmor ? ConfigParams.ArmorAdvancedVova : ConfigParams.ArmorBasicVova,
                medicineChestsCount,
                hasAim ? ConfigParams.AccuracyWithAimVova : ConfigParams.AccuracyBasicVova);
            BattleEngine.StateChanged += OnStateChanged;
            BattleEngine.MapChanged += OnMapChanged;
            BattleEngine.Shooted += OnShooted;
            UpdateVovaState();
            UpdateAvailableActions();
        }

        private void OnShooted(object sender, BattleModels.Events.ShootEventArgs e)
        {
            if (!e.GameEnded)
            {
                rtbLog.AppendText($"{e.Text}\r\n\r\n");
            }
            else
            {
                new FormInfoDialog(e.Text).ShowDialog();
                BattleEnded?.Invoke(this, new BattleEndedEventArgs(false));
            }
        }

        private void OnStateChanged(object sender, BattleModels.Events.StateChangedEventArgs e)
        {
            UpdateVovaState();
            UpdateAvailableActions();
        }

        private void BattleGameControl_Load(object sender, EventArgs e)
        {
            foreach (Control c in Controls)
            {
                if (c.Name != nameof(lbxBattleChoice))
                    c.KeyDown += new KeyEventHandler(BattleControlOnKeyDown);
            }
        }

        private void OnMapChanged(object sender, BattleModels.Events.MapChangedEventArgs e)
        {
            RedrawMap();
            UpdateAvailableActions();
        }

        private void UpdateVovaState()
        {
            lblHealthVal.Text = BattleEngine.Hp.ToString();
            lblArmorVal.Text = BattleEngine.Armor.ToString();
            lblMedKitCountVal.Text = BattleEngine.MedKits.ToString();
            lblAccuracyVal.Text = BattleEngine.Accuracy.ToString();
            lblGrenadesCountVal.Text = BattleEngine.Grenades.ToString();
        }

        private void UpdateAvailableActions()
        {
            HideAllMoveButtons();
            BattleEngine.UpdateSeenEnemiesCount();
            List<DirectionEnum> moves = BattleEngine.AvailableMoves;
            if (moves.Contains(DirectionEnum.Up))
                btnUp.Visible = true;
            if (moves.Contains(DirectionEnum.Down))
                btnDown.Visible = true;
            if (moves.Contains(DirectionEnum.Left))
                btnLeft.Visible = true;
            if (moves.Contains(DirectionEnum.Right))
                btnRight.Visible = true;

            lbxBattleChoice.Items.Clear();
            lbxBattleChoice.Items.Add("Ждать");
            if (BattleEngine.MedKits > 0)
                lbxBattleChoice.Items.Add("Воспользоваться аптечкой");
            if (BattleEngine.Grenades > 0)
                lbxBattleChoice.Items.Add("Оглушить врагов гранатой");
            for (int i = 0; i < BattleEngine.SeenEnemiesCount; i++)
            {
                if (i == 0)
                    lbxBattleChoice.Items.Add("Открыть огонь по ближайшему противнику");
                else if (i == 1)
                    lbxBattleChoice.Items.Add("Открыть огонь по противнику подальше");
                else if (i == 2)
                    lbxBattleChoice.Items.Add("Открыть огонь по противнику ещё подальше");
                else if (i == 3)
                    lbxBattleChoice.Items.Add("Открыть огонь по противнику ещё дальше");
            }
        }

        private void HideAllMoveButtons()
        {
            btnUp.Visible = false;
            btnDown.Visible = false;
            btnLeft.Visible = false;
            btnRight.Visible = false;
        }

        private void RedrawMap()
        {
            pbxMap.Load(_pathToPicture);
            if (GridVisible)
                DrawGrid();
            DrawAllStates();
        }

        private void DrawAllStates()
        {
            for (int i = 0; i < Map.SizeX; i++)
            {
                for (int j = 0; j < Map.SizeY; j++)
                {
                    if (Map[i, j].State != CellStateEnum.Empty)
                        DrawCell(Map[i, j]);
                }
            }
        }

        private void DrawCell(Cell cell)
        {
            if (cell.State == CellStateEnum.Wall && WallsVisible)
            {
                Drawing.FillRectangle(pbxMap, cell.Position.X * CellSizePx, cell.Position.Y * CellSizePx,
                    (cell.Position.X + 1) * CellSizePx, (cell.Position.Y + 1) * CellSizePx, Color.DarkSlateGray);
            }
            else if (cell.State == CellStateEnum.Vova)
            {
                Drawing.FillCircle(pbxMap, cell.Position.X * CellSizePx + CellSizePx / 2, 
                    cell.Position.Y * CellSizePx + CellSizePx / 2, 
                    HeroCircleRadius, lblLegendVovaColor.ForeColor);
            }
            else if (cell.State == CellStateEnum.Serega)
            {
                Drawing.FillCircle(pbxMap, cell.Position.X * CellSizePx + CellSizePx / 2,
                    cell.Position.Y * CellSizePx + CellSizePx / 2,
                    HeroCircleRadius, lblLegendSeregaColor.ForeColor);
            }
            else if (cell.State == CellStateEnum.Masha)
            {
                Drawing.FillCircle(pbxMap, cell.Position.X * CellSizePx + CellSizePx / 2,
                    cell.Position.Y * CellSizePx + CellSizePx / 2,
                    HeroCircleRadius, lblLegendMashaColor.ForeColor);
            }
            else if (cell.State == CellStateEnum.Sonya)
            {
                Drawing.FillCircle(pbxMap, cell.Position.X * CellSizePx + CellSizePx / 2,
                    cell.Position.Y * CellSizePx + CellSizePx / 2,
                    HeroCircleRadius, lblLegendSonyaColor.ForeColor);
            }
            else if (cell.State == CellStateEnum.Guard)
            {
                Drawing.FillCircle(pbxMap, cell.Position.X * CellSizePx + CellSizePx / 2,
                    cell.Position.Y * CellSizePx + CellSizePx / 2,
                    HeroCircleRadius, lblLegendGuardColor.ForeColor);
            }
            else if (cell.State == CellStateEnum.Finish)
            {
                Drawing.FillRectangle(pbxMap, cell.Position.X * CellSizePx, cell.Position.Y * CellSizePx,
                    (cell.Position.X + 1) * CellSizePx, (cell.Position.Y + 1) * CellSizePx, lblLegendFinishColor.ForeColor);
            }
        }

        private void btnShout_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(cbxInput.Text))
                {
                    rtbLog.AppendText(HandleCommand(cbxInput.Text.Trim()) + "\r\n\r\n");
                    cbxInput.Text = "";
                }
            }
            catch (Exception ex)
            {
                rtbLog.AppendText($"Ошибка:\r\n{ex.Message}\r\n\r\n");
            }
        }

        private string HandleCommand(string message)
        {
            string command = message.ToLower();
            string response = $"Вы кричите:\r\n— {message}";
            if (command.Contains("$help"))
            {
                response = $">{message}\r\nКоманды отладки:\r\n" +
                           $"$ShowWalls - показать реальные стены\r\n" +
                           $"$HideWalls - скрыть реальные стены\r\n" +
                           $"$ShowGrid - показать сетку\r\n" +
                           $"$HideGrid - спрятать сетку\r\n" +
                           $"$SetHP значение_здоровья - установить заданное значение здоровья\r\n" +
                           $"$SetArmor значение_брони - установить заданное значение брони\r\n" +
                           $"$SetMed количество_аптечек - установить заданное количество аптечек\r\n" +
                           $"$SetAc значение_точности - установить заданное значение точности\r\n" +
                           $"$SetGren количество_гранат - установить заданное количество гранат.";
            }
            else if (command.Contains("$showwalls"))
            {
                response = $">{message}\r\nОтображение реальных стен включено.";
                WallsVisible = true;
                RedrawMap();
            }
            else if (command.Contains("$hidewalls"))
            {
                response = $">{message}\r\nОтображение реальных стен выключено.";
                WallsVisible = false;
                RedrawMap();
            }
            else if (command.Contains("$showgrid"))
            {
                response = $">{message}\r\nОтображение игровой сетки включено.";
                GridVisible = true;
                RedrawMap();
            }
            else if (command.Contains("$hidegrid"))
            {
                response = $">{message}\r\nОтображение игровой сетки выключено.";
                GridVisible = false;
                RedrawMap();
            }
            else if (command.Contains("$sethp"))
            {
                int hp = int.Parse(message.Split(' ').Last());
                if (hp > 0)
                {
                    BattleEngine.Hp = hp;
                    response = $">{message}\r\nВеличина здоровья установлена в значение {hp}.";
                }
                else
                {
                    response = $">{message}\r\nВеличина здоровья должна быть положительной.";
                }
            }
            else if (command.Contains("setarmor"))
            {
                int armor = int.Parse(message.Split(' ').Last());
                if (armor >= 0)
                {
                    BattleEngine.Armor = armor;
                    response = $">{message}\r\nВеличина брони установлена в значение {armor}.";
                }
                else
                {
                    response = $">{message}\r\nВеличина брони должна быть неотрицательной.";
                }
            }
            else if (command.Contains("setmed"))
            {
                int medPacks = int.Parse(message.Split(' ').Last());
                if (medPacks >= 0)
                {
                    BattleEngine.MedKits = medPacks;
                    response = $">{message}\r\nВеличина аптечек установлена в значение {medPacks}.";
                }
                else
                {
                    response = $">{message}\r\nВеличина аптечек должна быть неотрицательной.";
                }
            }
            else if (command.Contains("setac"))
            {
                int accuracy = int.Parse(message.Split(' ').Last());
                if (accuracy > 0)
                {
                    BattleEngine.Accuracy = accuracy;
                    response = $">{message}\r\nВеличина точности установлена в значение {accuracy}.";
                }
                else
                {
                    response = $">{message}\r\nВеличина точности должна быть положительной.";
                }
            }
            else if (command.Contains("setgren"))
            {
                int grenades = int.Parse(message.Split(' ').Last());
                if (grenades >= 0)
                {
                    BattleEngine.Grenades = grenades;
                    response = $">{message}\r\nВеличина гранат установлена в значение {grenades}.";
                }
                else
                {
                    response = $">{message}\r\nВеличина гранат должна быть неотрицательной.";
                }
            }
            else if (command.Contains("сдаюсь"))
            {
                string text = "Вы осознаёте, что это бой вам уже не выиграть. Сопротивление бесполезно, вы окружены превосходящими силами противника.\r\n" +
                              "И вы принимаете единственное верное в данной ситуации решение — сдаться.\r\n" +
                              "Вы бросаете оружие и ложитесь на пол. Вы надеетесь, что вам дадут не слишком большой срок в Крарковской тюрьме.\r\n" +
                              "Но один вопрос не даёт вам покоя: \"А смогли бы вы победить, действуя в бою иначе? " +
                              "Или выиграть нереально и стоило бы попросить о помощи Денчика?\"\r\n" +
                              "Пока вы погружены в свои мысли, вас с подельниками \"пакуют\" и грузят в автозак. В следующий раз воюйте лучше!";
                new FormInfoDialog(text).ShowDialog();
                BattleEnded?.Invoke(this, new BattleEndedEventArgs(false));
            }
            else if (command.Contains("ден"))
            {
                string text = "Вы уже было отчаялись, но прямо в разгар боя откуда ни возьмись рядом с вами вы замечаете спокойно стоящего Денчика. " +
                              "Странно, как он вообще здесь оказался?!\r\nДенчик, улыбаясь, смотрит на вас и с загадочным видом произносит:\r\n " +
                              "— Ложки нет.\r\nЧто бы это могло значить?\r\nДенчик тем временем не спеша надевает странную перчатку и продолжает:\r\n" +
                              "— Прикольно у вас тут в Краркове. Но мне пора, меня Вика в Doom'е ждёт.\r\n" +
                              "После этого он щёлкает пальцами, за ним открывается портал и он скрывается в нём.\r\n" +
                              "Вы не успеваете понять, что произошло, как все враги c хрипом и матом вокруг вас начинают растворяться в воздухе, " +
                              "а вы с друзьями, живые и невредимые, оказываетесь у точки сбора.\r\n" +
                              "Спасибо, Денчик!";
                new FormInfoDialog(text).ShowDialog();
                BattleEnded?.Invoke(this, new BattleEndedEventArgs(true));
            }
            else if (command.Contains("серчик, я справлюсь"))
            {
                BattleEngine.ChangeStrategy(HeroEnum.Serega, HeroStrategy.MoveToFinish);
                if (BattleEngine.IsPresent(HeroEnum.Serega))
                    response += $"\r\nВы слышите, как Серчик кричит:\r\n— Принято! Жду у точки!";
                else
                    response += $"\r\nНо увы, он уже съебался.";
            }
            else if (command.Contains("сер") || command.Contains("брат"))
            {
                BattleEngine.ChangeStrategy(HeroEnum.Serega, HeroStrategy.MoveToVova);
                if (BattleEngine.IsPresent(HeroEnum.Serega))
                    response += $"\r\nВ ответ вы слышите Серчика:\r\n— Жди, я на подлёте!";
                else
                    response += $"\r\nНо увы, он уже съебался.";
            }
            else if (command.Contains("мавыка, у меня всё под контролем"))
            {
                BattleEngine.ChangeStrategy(HeroEnum.Masha, HeroStrategy.MoveToFinish);
                if (BattleEngine.IsPresent(HeroEnum.Masha))
                    response += $"\r\nВ ответ вы слышите крик Мавыки:\r\n— С удовольствием! Я съёбываю!";
                else
                    response += $"\r\nНо увы, она уже съебалась.";
            }
            else if (command.Contains("мавык") || command.Contains("маш"))
            {
                BattleEngine.ChangeStrategy(HeroEnum.Masha, HeroStrategy.MoveToVova);
                if (BattleEngine.IsPresent(HeroEnum.Masha))
                    response += $"\r\nСпотыкаясь на бегу, Мавыка кричит вам:\r\n— Машина смерти спешит на помощь!";
                else
                    response += $"\r\nНо увы, она уже съебалась.";
            }
            else if (command.Contains("фофарик, беги"))
            {
                BattleEngine.ChangeStrategy(HeroEnum.Sonya, HeroStrategy.MoveToFinish);
                if (BattleEngine.IsPresent(HeroEnum.Sonya))
                    response += $"\r\nВы слышите, как Фофуся кричит вам в ответ:\r\n— Оки! Встретимся у точки!";
                else
                    response += $"\r\nНо увы, она уже съебалась.";
            }
            else if (command.Contains("фоф") || command.Contains("сон"))
            {
                BattleEngine.ChangeStrategy(HeroEnum.Sonya, HeroStrategy.MoveToVova);
                if (BattleEngine.IsPresent(HeroEnum.Sonya))
                    response += $"\r\nСквозь звуки битвы вы слышите Соню:\r\n— Держись! Сейчас вместе всех разъебём!";
                else
                    response += $"\r\nНо увы, она уже съебалась.";
            }
            else if (command.Contains("пидор") || command.Contains("педик") || command.Contains("урод") || command.Contains("коз")
                     || command.Contains("твар") || command.Contains("дурак") || command.Contains("сос") || command.Contains("еба")
                     || command.Contains("чмо") || command.Contains("мудак") || command.Contains("пидр") || command.Contains("геи")
                     || command.Contains("гей") || command.Contains("мраз") || command.Contains("ебл") || command.Contains("дохни")
                     || command.Contains("бля") || command.Contains("ху") || command.Contains("пизд") || command.Contains("хер")
                     || command.Contains("сук") || command.Contains("сволоч"))
            {
                BattleEngine.ChangeStrategy(HeroEnum.Guard, HeroStrategy.MoveToVova);
                response += $"\r\nВот теперь вы их по-настоящему разозлили! Держитесь, вся толпа врагов теперь бежит на вас!";
            }
            else if (command.Contains("отвлеките"))
            {
                BattleEngine.ChangeStrategy(HeroEnum.Guard, HeroStrategy.MoveToNearestGoodGuy);
                response += $"\r\nУслышав вас, ваши друзья-сообщники самоотверженно берут огонь на себя. Теперь все враги бегут на них!";
            }
            else
            {
                string[] answers =
                {
                    "Чё мля!?!", "Чииво бля!?!", "Чего!?", "Што нахуй!?!", "А!?", "Не слышу бля!", "Не ори!", "Кто здесь!?!",
                    "Пиздец!", "Ну охуеть теперь!", "ААААА!!!", "Суууука!", "Не стреляйте, меня дома толстая жена ждёт!", 
                    "Попизди мне тут!", "Вот сейчас давай!", "Окружай их!", "Он здесь, падла!", "Выживу, сделаю Людке предложение!",
                    "А в тюрьме сейчас макароны!", "Всех убью, один останусь!", "Ебаааать!", "Пацаны, кто кричал?", "Мужики, окружай!"
                };
                string answer = answers[new Random().Next(answers.Length)];
                response += $"\r\nНа что сквозь рокот автоматных очередей вы слышите:\r\n— {answer}";
            }

            return response;
        }

        private void cbxInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                btnShout.PerformClick();
            }
        }

        private void rtbLog_TextChanged(object sender, EventArgs e)
        {
            rtbLog.SelectionStart = rtbLog.Text.Length;
            rtbLog.ScrollToCaret();
        }

        private void DrawGrid()
        {
            int x = 0, y = 0;

            while (x < pbxMap.Width)
            {
                Drawing.DrawLine(pbxMap, x, 0, x, pbxMap.Height, 1, Color.Gray);
                x += CellSizePx;
            }

            while (y < pbxMap.Height)
            {
                Drawing.DrawLine(pbxMap, 0, y, pbxMap.Width, y, 1, Color.Gray);
                y += CellSizePx;
            }
        }

        private void Move(DirectionEnum direction)
        {
            if (BattleEngine.Move(direction) == CellStateEnum.Finish)
            {
                if (!BattleEngine.IsPresent(HeroEnum.Serega) && !BattleEngine.IsPresent(HeroEnum.Masha) 
                                                             && !BattleEngine.IsPresent(HeroEnum.Sonya))
                    BattleEnded?.Invoke(this, new BattleEndedEventArgs(true));
            }
            BattleEngine.DoOtherHeroesTurn();
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            Move(DirectionEnum.Up);
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            Move(DirectionEnum.Down);
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            Move(DirectionEnum.Left);
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            Move(DirectionEnum.Right);
        }

        private void BattleControlOnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up && btnUp.Visible)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                btnUp.PerformClick();
            }
            else if (e.KeyCode == Keys.Down && btnDown.Visible)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                btnDown.PerformClick();
            }
            else if (e.KeyCode == Keys.Left && btnLeft.Visible)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                btnLeft.PerformClick();
            }
            else if (e.KeyCode == Keys.Right && btnRight.Visible)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                btnRight.PerformClick();
            }
        }

        private void lbxBattleChoice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                ChoiceBattleOption(lbxBattleChoice.SelectedItem?.ToString());
            }
        }

        private void ChoiceBattleOption(string command)
        {
            if (!string.IsNullOrWhiteSpace(command))
            {
                if (command == "Ждать")
                {
                    rtbLog.AppendText("Вы решаете выдержать тактическую паузу и позволить противникам совершить смертельную ошибку.\r\n\r\n");
                    BattleEngine.DoOtherHeroesTurn();
                }
                else if (command == "Воспользоваться аптечкой")
                {
                    BattleEngine.Hp += ConfigParams.MedKitEffect;
                    if (BattleEngine.Hp > ConfigParams.HpVova)
                        BattleEngine.Hp = ConfigParams.HpVova;
                    BattleEngine.MedKits--;
                    UpdateAvailableActions();
                    rtbLog.AppendText("Вы набегу вскрываете аптечку, глотаете пилюли (лишь бы это было не слабительное!), перевязываете раны " +
                                      "и вкалываете себе обезболивающее с антибиотиком. Так-то лучше! Теперь можно снова рвать задницы!\r\n\r\n");
                }
                else if (command == "Оглушить врагов гранатой")
                {
                    if (BattleEngine.Grenades > 0)
                    {
                        BattleEngine.ThrowGrenade();
                        rtbLog.AppendText($"Поняв, что дело пахнет керосином, вы решаете дать себе и своим друзьям передышку. " +
                                          $"Усмехаясь, вы бросаете в толпу врагов светошумовую гранату, зная, что она устроит им весёлую жизнь " +
                                          $"на {ConfigParams.GrenadeEffect} мгновения. Однако, враги ваш юмор не оценили и не " +
                                          $"\"видят\" в нём ничего смешного!\r\n\r\n");
                    }
                }
                else if (command == "Открыть огонь по ближайшему противнику")
                {
                    BattleEngine.Shoot(0);
                    BattleEngine.DoOtherHeroesTurn();
                }
                else if (command == "Открыть огонь по противнику подальше")
                {
                    BattleEngine.Shoot(1);
                    BattleEngine.DoOtherHeroesTurn();
                }
                else if (command == "Открыть огонь по противнику ещё подальше")
                {
                    BattleEngine.Shoot(2);
                    BattleEngine.DoOtherHeroesTurn();
                }
                else if (command == "Открыть огонь по противнику ещё дальше")
                {
                    BattleEngine.Shoot(3);
                    BattleEngine.DoOtherHeroesTurn();
                }
            }
        }

        private void lbxBattleChoice_DoubleClick(object sender, EventArgs e)
        {
            ChoiceBattleOption(lbxBattleChoice.SelectedItem?.ToString());
        }

        private void BattleGameControl_Click(object sender, EventArgs e)
        {
            rtbLog.Focus();
        }
    }
}
