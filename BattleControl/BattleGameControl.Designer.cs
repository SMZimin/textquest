﻿namespace BattleControl
{
    partial class BattleGameControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BattleGameControl));
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.btnShout = new System.Windows.Forms.Button();
            this.tblpMoveButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.pnlMoveButtons = new System.Windows.Forms.Panel();
            this.pnlStates = new System.Windows.Forms.Panel();
            this.lblGrenadesCountVal = new System.Windows.Forms.Label();
            this.lblGrenadesCount = new System.Windows.Forms.Label();
            this.lblMedKitCountVal = new System.Windows.Forms.Label();
            this.lblMedKitCount = new System.Windows.Forms.Label();
            this.lblAccuracyVal = new System.Windows.Forms.Label();
            this.lblHealthVal = new System.Windows.Forms.Label();
            this.lblArmorVal = new System.Windows.Forms.Label();
            this.lblAccuracy = new System.Windows.Forms.Label();
            this.lblHealth = new System.Windows.Forms.Label();
            this.lblArmor = new System.Windows.Forms.Label();
            this.pbxMap = new System.Windows.Forms.PictureBox();
            this.cbxInput = new System.Windows.Forms.ComboBox();
            this.pnlLegend = new System.Windows.Forms.Panel();
            this.lblLegendFinish = new System.Windows.Forms.Label();
            this.lblLegendFinishColor = new System.Windows.Forms.Label();
            this.lblLegendGuard = new System.Windows.Forms.Label();
            this.lblLegendGuardColor = new System.Windows.Forms.Label();
            this.lblLegendSonya = new System.Windows.Forms.Label();
            this.lblLegendSonyaColor = new System.Windows.Forms.Label();
            this.lblLegendMasha = new System.Windows.Forms.Label();
            this.lblLegendMashaColor = new System.Windows.Forms.Label();
            this.lblLegendSerega = new System.Windows.Forms.Label();
            this.lblLegendSeregaColor = new System.Windows.Forms.Label();
            this.lblLegendVova = new System.Windows.Forms.Label();
            this.lblLegendVovaColor = new System.Windows.Forms.Label();
            this.lbxBattleChoice = new System.Windows.Forms.ListBox();
            this.tblpMoveButtons.SuspendLayout();
            this.pnlMoveButtons.SuspendLayout();
            this.pnlStates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMap)).BeginInit();
            this.pnlLegend.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbLog
            // 
            this.rtbLog.BackColor = System.Drawing.Color.DarkSlateGray;
            this.rtbLog.Font = new System.Drawing.Font("Corbel Light", 18F, System.Drawing.FontStyle.Italic);
            this.rtbLog.ForeColor = System.Drawing.SystemColors.Window;
            this.rtbLog.Location = new System.Drawing.Point(32, 32);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.ReadOnly = true;
            this.rtbLog.Size = new System.Drawing.Size(792, 614);
            this.rtbLog.TabIndex = 6;
            this.rtbLog.Text = "";
            this.rtbLog.TextChanged += new System.EventHandler(this.rtbLog_TextChanged);
            // 
            // btnShout
            // 
            this.btnShout.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnShout.Location = new System.Drawing.Point(726, 652);
            this.btnShout.Name = "btnShout";
            this.btnShout.Size = new System.Drawing.Size(98, 37);
            this.btnShout.TabIndex = 8;
            this.btnShout.Text = "Крикнуть";
            this.btnShout.UseVisualStyleBackColor = true;
            this.btnShout.Click += new System.EventHandler(this.btnShout_Click);
            // 
            // tblpMoveButtons
            // 
            this.tblpMoveButtons.ColumnCount = 3;
            this.tblpMoveButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblpMoveButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblpMoveButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblpMoveButtons.Controls.Add(this.btnRight, 2, 1);
            this.tblpMoveButtons.Controls.Add(this.btnLeft, 0, 1);
            this.tblpMoveButtons.Controls.Add(this.btnUp, 1, 0);
            this.tblpMoveButtons.Controls.Add(this.btnDown, 1, 2);
            this.tblpMoveButtons.Location = new System.Drawing.Point(12, 12);
            this.tblpMoveButtons.Name = "tblpMoveButtons";
            this.tblpMoveButtons.RowCount = 3;
            this.tblpMoveButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblpMoveButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblpMoveButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblpMoveButtons.Size = new System.Drawing.Size(301, 301);
            this.tblpMoveButtons.TabIndex = 10;
            // 
            // btnRight
            // 
            this.btnRight.Font = new System.Drawing.Font("Corbel", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRight.Location = new System.Drawing.Point(203, 103);
            this.btnRight.Name = "btnRight";
            this.btnRight.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.btnRight.Size = new System.Drawing.Size(95, 94);
            this.btnRight.TabIndex = 12;
            this.btnRight.Text = "🠆";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Font = new System.Drawing.Font("Corbel", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnLeft.Location = new System.Drawing.Point(3, 103);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnLeft.Size = new System.Drawing.Size(94, 94);
            this.btnLeft.TabIndex = 12;
            this.btnLeft.Text = "🠄 ";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnUp
            // 
            this.btnUp.Font = new System.Drawing.Font("Corbel", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnUp.Location = new System.Drawing.Point(103, 3);
            this.btnUp.Name = "btnUp";
            this.btnUp.Padding = new System.Windows.Forms.Padding(12, 0, 0, 8);
            this.btnUp.Size = new System.Drawing.Size(94, 94);
            this.btnUp.TabIndex = 12;
            this.btnUp.Text = "🠅";
            this.btnUp.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDown
            // 
            this.btnDown.Font = new System.Drawing.Font("Corbel", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDown.Location = new System.Drawing.Point(103, 203);
            this.btnDown.Name = "btnDown";
            this.btnDown.Padding = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.btnDown.Size = new System.Drawing.Size(94, 95);
            this.btnDown.TabIndex = 13;
            this.btnDown.Text = "🠋";
            this.btnDown.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // pnlMoveButtons
            // 
            this.pnlMoveButtons.BackColor = System.Drawing.Color.DarkSlateGray;
            this.pnlMoveButtons.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMoveButtons.Controls.Add(this.tblpMoveButtons);
            this.pnlMoveButtons.Location = new System.Drawing.Point(855, 722);
            this.pnlMoveButtons.Name = "pnlMoveButtons";
            this.pnlMoveButtons.Size = new System.Drawing.Size(327, 327);
            this.pnlMoveButtons.TabIndex = 11;
            // 
            // pnlStates
            // 
            this.pnlStates.BackColor = System.Drawing.Color.DarkSlateGray;
            this.pnlStates.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlStates.Controls.Add(this.lblGrenadesCountVal);
            this.pnlStates.Controls.Add(this.lblGrenadesCount);
            this.pnlStates.Controls.Add(this.lblMedKitCountVal);
            this.pnlStates.Controls.Add(this.lblMedKitCount);
            this.pnlStates.Controls.Add(this.lblAccuracyVal);
            this.pnlStates.Controls.Add(this.lblHealthVal);
            this.pnlStates.Controls.Add(this.lblArmorVal);
            this.pnlStates.Controls.Add(this.lblAccuracy);
            this.pnlStates.Controls.Add(this.lblHealth);
            this.pnlStates.Controls.Add(this.lblArmor);
            this.pnlStates.Location = new System.Drawing.Point(1190, 722);
            this.pnlStates.Name = "pnlStates";
            this.pnlStates.Size = new System.Drawing.Size(701, 327);
            this.pnlStates.TabIndex = 12;
            // 
            // lblGrenadesCountVal
            // 
            this.lblGrenadesCountVal.AutoSize = true;
            this.lblGrenadesCountVal.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblGrenadesCountVal.ForeColor = System.Drawing.SystemColors.Window;
            this.lblGrenadesCountVal.Location = new System.Drawing.Point(267, 249);
            this.lblGrenadesCountVal.Name = "lblGrenadesCountVal";
            this.lblGrenadesCountVal.Size = new System.Drawing.Size(50, 59);
            this.lblGrenadesCountVal.TabIndex = 22;
            this.lblGrenadesCountVal.Text = "0";
            // 
            // lblGrenadesCount
            // 
            this.lblGrenadesCount.AutoSize = true;
            this.lblGrenadesCount.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblGrenadesCount.ForeColor = System.Drawing.SystemColors.Window;
            this.lblGrenadesCount.Location = new System.Drawing.Point(16, 249);
            this.lblGrenadesCount.Name = "lblGrenadesCount";
            this.lblGrenadesCount.Size = new System.Drawing.Size(215, 59);
            this.lblGrenadesCount.TabIndex = 21;
            this.lblGrenadesCount.Text = "Гранаты:";
            // 
            // lblMedKitCountVal
            // 
            this.lblMedKitCountVal.AutoSize = true;
            this.lblMedKitCountVal.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMedKitCountVal.ForeColor = System.Drawing.SystemColors.Window;
            this.lblMedKitCountVal.Location = new System.Drawing.Point(267, 192);
            this.lblMedKitCountVal.Name = "lblMedKitCountVal";
            this.lblMedKitCountVal.Size = new System.Drawing.Size(50, 59);
            this.lblMedKitCountVal.TabIndex = 20;
            this.lblMedKitCountVal.Text = "0";
            // 
            // lblMedKitCount
            // 
            this.lblMedKitCount.AutoSize = true;
            this.lblMedKitCount.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblMedKitCount.ForeColor = System.Drawing.SystemColors.Window;
            this.lblMedKitCount.Location = new System.Drawing.Point(16, 192);
            this.lblMedKitCount.Name = "lblMedKitCount";
            this.lblMedKitCount.Size = new System.Drawing.Size(218, 59);
            this.lblMedKitCount.TabIndex = 19;
            this.lblMedKitCount.Text = "Аптечки:";
            // 
            // lblAccuracyVal
            // 
            this.lblAccuracyVal.AutoSize = true;
            this.lblAccuracyVal.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAccuracyVal.ForeColor = System.Drawing.SystemColors.Window;
            this.lblAccuracyVal.Location = new System.Drawing.Point(267, 133);
            this.lblAccuracyVal.Name = "lblAccuracyVal";
            this.lblAccuracyVal.Size = new System.Drawing.Size(50, 59);
            this.lblAccuracyVal.TabIndex = 18;
            this.lblAccuracyVal.Text = "0";
            // 
            // lblHealthVal
            // 
            this.lblHealthVal.AutoSize = true;
            this.lblHealthVal.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHealthVal.ForeColor = System.Drawing.SystemColors.Window;
            this.lblHealthVal.Location = new System.Drawing.Point(267, 74);
            this.lblHealthVal.Name = "lblHealthVal";
            this.lblHealthVal.Size = new System.Drawing.Size(50, 59);
            this.lblHealthVal.TabIndex = 17;
            this.lblHealthVal.Text = "0";
            // 
            // lblArmorVal
            // 
            this.lblArmorVal.AutoSize = true;
            this.lblArmorVal.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblArmorVal.ForeColor = System.Drawing.SystemColors.Window;
            this.lblArmorVal.Location = new System.Drawing.Point(267, 15);
            this.lblArmorVal.Name = "lblArmorVal";
            this.lblArmorVal.Size = new System.Drawing.Size(50, 59);
            this.lblArmorVal.TabIndex = 16;
            this.lblArmorVal.Text = "0";
            // 
            // lblAccuracy
            // 
            this.lblAccuracy.AutoSize = true;
            this.lblAccuracy.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAccuracy.ForeColor = System.Drawing.SystemColors.Window;
            this.lblAccuracy.Location = new System.Drawing.Point(16, 133);
            this.lblAccuracy.Name = "lblAccuracy";
            this.lblAccuracy.Size = new System.Drawing.Size(233, 59);
            this.lblAccuracy.TabIndex = 15;
            this.lblAccuracy.Text = "Точность:";
            // 
            // lblHealth
            // 
            this.lblHealth.AutoSize = true;
            this.lblHealth.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHealth.ForeColor = System.Drawing.SystemColors.Window;
            this.lblHealth.Location = new System.Drawing.Point(16, 74);
            this.lblHealth.Name = "lblHealth";
            this.lblHealth.Size = new System.Drawing.Size(245, 59);
            this.lblHealth.TabIndex = 14;
            this.lblHealth.Text = "Здоровье:";
            // 
            // lblArmor
            // 
            this.lblArmor.AutoSize = true;
            this.lblArmor.Font = new System.Drawing.Font("Corbel", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblArmor.ForeColor = System.Drawing.SystemColors.Window;
            this.lblArmor.Location = new System.Drawing.Point(16, 15);
            this.lblArmor.Name = "lblArmor";
            this.lblArmor.Size = new System.Drawing.Size(173, 59);
            this.lblArmor.TabIndex = 13;
            this.lblArmor.Text = "Броня:";
            // 
            // pbxMap
            // 
            this.pbxMap.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbxMap.Image = ((System.Drawing.Image)(resources.GetObject("pbxMap.Image")));
            this.pbxMap.Location = new System.Drawing.Point(855, 32);
            this.pbxMap.Name = "pbxMap";
            this.pbxMap.Size = new System.Drawing.Size(1036, 614);
            this.pbxMap.TabIndex = 13;
            this.pbxMap.TabStop = false;
            // 
            // cbxInput
            // 
            this.cbxInput.Font = new System.Drawing.Font("Corbel", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbxInput.FormattingEnabled = true;
            this.cbxInput.Items.AddRange(new object[] {
            "Серчик, прикрой меня!",
            "Серчик, я справлюсь! Беги!",
            "Мавыка, запрашиваю твою поддержку!",
            "Мавыка, у меня всё под контролем! Беги к точке!",
            "Фофарик, поддержи огнём!",
            "Фофарик, беги, я прикрою!",
            "Эй пидоры! Я тут!",
            "Ребят, отвлеките их!",
            "Не стреляйте, я сдаюсь!",
            "Денчик, спаси! Я совсем отчаялся!",
            "$help (потом скрыть нужно будет)"});
            this.cbxInput.Location = new System.Drawing.Point(32, 652);
            this.cbxInput.Name = "cbxInput";
            this.cbxInput.Size = new System.Drawing.Size(688, 37);
            this.cbxInput.TabIndex = 14;
            this.cbxInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbxInput_KeyDown);
            // 
            // pnlLegend
            // 
            this.pnlLegend.BackColor = System.Drawing.Color.DarkSlateGray;
            this.pnlLegend.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlLegend.Controls.Add(this.lblLegendFinish);
            this.pnlLegend.Controls.Add(this.lblLegendFinishColor);
            this.pnlLegend.Controls.Add(this.lblLegendGuard);
            this.pnlLegend.Controls.Add(this.lblLegendGuardColor);
            this.pnlLegend.Controls.Add(this.lblLegendSonya);
            this.pnlLegend.Controls.Add(this.lblLegendSonyaColor);
            this.pnlLegend.Controls.Add(this.lblLegendMasha);
            this.pnlLegend.Controls.Add(this.lblLegendMashaColor);
            this.pnlLegend.Controls.Add(this.lblLegendSerega);
            this.pnlLegend.Controls.Add(this.lblLegendSeregaColor);
            this.pnlLegend.Controls.Add(this.lblLegendVova);
            this.pnlLegend.Controls.Add(this.lblLegendVovaColor);
            this.pnlLegend.Location = new System.Drawing.Point(855, 653);
            this.pnlLegend.Name = "pnlLegend";
            this.pnlLegend.Size = new System.Drawing.Size(1036, 36);
            this.pnlLegend.TabIndex = 15;
            // 
            // lblLegendFinish
            // 
            this.lblLegendFinish.AutoSize = true;
            this.lblLegendFinish.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendFinish.ForeColor = System.Drawing.SystemColors.Window;
            this.lblLegendFinish.Location = new System.Drawing.Point(744, 4);
            this.lblLegendFinish.Name = "lblLegendFinish";
            this.lblLegendFinish.Size = new System.Drawing.Size(113, 23);
            this.lblLegendFinish.TabIndex = 17;
            this.lblLegendFinish.Text = "Точка сбора";
            // 
            // lblLegendFinishColor
            // 
            this.lblLegendFinishColor.AutoSize = true;
            this.lblLegendFinishColor.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendFinishColor.ForeColor = System.Drawing.Color.White;
            this.lblLegendFinishColor.Location = new System.Drawing.Point(712, 4);
            this.lblLegendFinishColor.Name = "lblLegendFinishColor";
            this.lblLegendFinishColor.Size = new System.Drawing.Size(23, 23);
            this.lblLegendFinishColor.TabIndex = 16;
            this.lblLegendFinishColor.Text = "⬛";
            // 
            // lblLegendGuard
            // 
            this.lblLegendGuard.AutoSize = true;
            this.lblLegendGuard.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendGuard.ForeColor = System.Drawing.SystemColors.Window;
            this.lblLegendGuard.Location = new System.Drawing.Point(601, 5);
            this.lblLegendGuard.Name = "lblLegendGuard";
            this.lblLegendGuard.Size = new System.Drawing.Size(74, 23);
            this.lblLegendGuard.TabIndex = 17;
            this.lblLegendGuard.Text = "Охрана";
            // 
            // lblLegendGuardColor
            // 
            this.lblLegendGuardColor.AutoSize = true;
            this.lblLegendGuardColor.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendGuardColor.ForeColor = System.Drawing.Color.Red;
            this.lblLegendGuardColor.Location = new System.Drawing.Point(569, 5);
            this.lblLegendGuardColor.Name = "lblLegendGuardColor";
            this.lblLegendGuardColor.Size = new System.Drawing.Size(31, 23);
            this.lblLegendGuardColor.TabIndex = 16;
            this.lblLegendGuardColor.Text = "⚫";
            // 
            // lblLegendSonya
            // 
            this.lblLegendSonya.AutoSize = true;
            this.lblLegendSonya.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendSonya.ForeColor = System.Drawing.SystemColors.Window;
            this.lblLegendSonya.Location = new System.Drawing.Point(445, 4);
            this.lblLegendSonya.Name = "lblLegendSonya";
            this.lblLegendSonya.Size = new System.Drawing.Size(89, 23);
            this.lblLegendSonya.TabIndex = 17;
            this.lblLegendSonya.Text = "Фофарик";
            // 
            // lblLegendSonyaColor
            // 
            this.lblLegendSonyaColor.AutoSize = true;
            this.lblLegendSonyaColor.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendSonyaColor.ForeColor = System.Drawing.Color.Fuchsia;
            this.lblLegendSonyaColor.Location = new System.Drawing.Point(413, 4);
            this.lblLegendSonyaColor.Name = "lblLegendSonyaColor";
            this.lblLegendSonyaColor.Size = new System.Drawing.Size(31, 23);
            this.lblLegendSonyaColor.TabIndex = 16;
            this.lblLegendSonyaColor.Text = "⚫";
            // 
            // lblLegendMasha
            // 
            this.lblLegendMasha.AutoSize = true;
            this.lblLegendMasha.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendMasha.ForeColor = System.Drawing.SystemColors.Window;
            this.lblLegendMasha.Location = new System.Drawing.Point(305, 5);
            this.lblLegendMasha.Name = "lblLegendMasha";
            this.lblLegendMasha.Size = new System.Drawing.Size(78, 23);
            this.lblLegendMasha.TabIndex = 5;
            this.lblLegendMasha.Text = "Мавыка";
            // 
            // lblLegendMashaColor
            // 
            this.lblLegendMashaColor.AutoSize = true;
            this.lblLegendMashaColor.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendMashaColor.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblLegendMashaColor.Location = new System.Drawing.Point(273, 5);
            this.lblLegendMashaColor.Name = "lblLegendMashaColor";
            this.lblLegendMashaColor.Size = new System.Drawing.Size(31, 23);
            this.lblLegendMashaColor.TabIndex = 4;
            this.lblLegendMashaColor.Text = "⚫";
            // 
            // lblLegendSerega
            // 
            this.lblLegendSerega.AutoSize = true;
            this.lblLegendSerega.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendSerega.ForeColor = System.Drawing.SystemColors.Window;
            this.lblLegendSerega.Location = new System.Drawing.Point(173, 4);
            this.lblLegendSerega.Name = "lblLegendSerega";
            this.lblLegendSerega.Size = new System.Drawing.Size(71, 23);
            this.lblLegendSerega.TabIndex = 3;
            this.lblLegendSerega.Text = "Серчик";
            // 
            // lblLegendSeregaColor
            // 
            this.lblLegendSeregaColor.AutoSize = true;
            this.lblLegendSeregaColor.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendSeregaColor.ForeColor = System.Drawing.Color.Green;
            this.lblLegendSeregaColor.Location = new System.Drawing.Point(141, 4);
            this.lblLegendSeregaColor.Name = "lblLegendSeregaColor";
            this.lblLegendSeregaColor.Size = new System.Drawing.Size(31, 23);
            this.lblLegendSeregaColor.TabIndex = 2;
            this.lblLegendSeregaColor.Text = "⚫";
            // 
            // lblLegendVova
            // 
            this.lblLegendVova.AutoSize = true;
            this.lblLegendVova.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendVova.ForeColor = System.Drawing.SystemColors.Window;
            this.lblLegendVova.Location = new System.Drawing.Point(35, 4);
            this.lblLegendVova.Name = "lblLegendVova";
            this.lblLegendVova.Size = new System.Drawing.Size(74, 23);
            this.lblLegendVova.TabIndex = 1;
            this.lblLegendVova.Text = "Вавыка";
            // 
            // lblLegendVovaColor
            // 
            this.lblLegendVovaColor.AutoSize = true;
            this.lblLegendVovaColor.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLegendVovaColor.ForeColor = System.Drawing.Color.BlueViolet;
            this.lblLegendVovaColor.Location = new System.Drawing.Point(3, 4);
            this.lblLegendVovaColor.Name = "lblLegendVovaColor";
            this.lblLegendVovaColor.Size = new System.Drawing.Size(31, 23);
            this.lblLegendVovaColor.TabIndex = 0;
            this.lblLegendVovaColor.Text = "⚫";
            // 
            // lbxBattleChoice
            // 
            this.lbxBattleChoice.BackColor = System.Drawing.SystemColors.Menu;
            this.lbxBattleChoice.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbxBattleChoice.FormattingEnabled = true;
            this.lbxBattleChoice.ItemHeight = 23;
            this.lbxBattleChoice.Location = new System.Drawing.Point(32, 722);
            this.lbxBattleChoice.Name = "lbxBattleChoice";
            this.lbxBattleChoice.Size = new System.Drawing.Size(792, 326);
            this.lbxBattleChoice.TabIndex = 16;
            this.lbxBattleChoice.DoubleClick += new System.EventHandler(this.lbxBattleChoice_DoubleClick);
            this.lbxBattleChoice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbxBattleChoice_KeyDown);
            // 
            // BattleGameControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lbxBattleChoice);
            this.Controls.Add(this.pnlLegend);
            this.Controls.Add(this.cbxInput);
            this.Controls.Add(this.pbxMap);
            this.Controls.Add(this.pnlStates);
            this.Controls.Add(this.pnlMoveButtons);
            this.Controls.Add(this.btnShout);
            this.Controls.Add(this.rtbLog);
            this.Name = "BattleGameControl";
            this.Size = new System.Drawing.Size(1918, 1078);
            this.Load += new System.EventHandler(this.BattleGameControl_Load);
            this.Click += new System.EventHandler(this.BattleGameControl_Click);
            this.tblpMoveButtons.ResumeLayout(false);
            this.pnlMoveButtons.ResumeLayout(false);
            this.pnlStates.ResumeLayout(false);
            this.pnlStates.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMap)).EndInit();
            this.pnlLegend.ResumeLayout(false);
            this.pnlLegend.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.Button btnShout;
        private System.Windows.Forms.TableLayoutPanel tblpMoveButtons;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Panel pnlMoveButtons;
        private System.Windows.Forms.Panel pnlStates;
        private System.Windows.Forms.Label lblAccuracy;
        private System.Windows.Forms.Label lblHealth;
        private System.Windows.Forms.Label lblArmor;
        private System.Windows.Forms.Label lblAccuracyVal;
        private System.Windows.Forms.Label lblHealthVal;
        private System.Windows.Forms.Label lblArmorVal;
        private System.Windows.Forms.PictureBox pbxMap;
        private System.Windows.Forms.Label lblMedKitCountVal;
        private System.Windows.Forms.Label lblMedKitCount;
        private System.Windows.Forms.Label lblGrenadesCountVal;
        private System.Windows.Forms.Label lblGrenadesCount;
        private System.Windows.Forms.ComboBox cbxInput;
        private System.Windows.Forms.Panel pnlLegend;
        private System.Windows.Forms.Label lblLegendFinish;
        private System.Windows.Forms.Label lblLegendFinishColor;
        private System.Windows.Forms.Label lblLegendGuard;
        private System.Windows.Forms.Label lblLegendGuardColor;
        private System.Windows.Forms.Label lblLegendSonya;
        private System.Windows.Forms.Label lblLegendSonyaColor;
        private System.Windows.Forms.Label lblLegendMasha;
        private System.Windows.Forms.Label lblLegendMashaColor;
        private System.Windows.Forms.Label lblLegendSerega;
        private System.Windows.Forms.Label lblLegendSeregaColor;
        private System.Windows.Forms.Label lblLegendVova;
        private System.Windows.Forms.Label lblLegendVovaColor;
        private System.Windows.Forms.ListBox lbxBattleChoice;
    }
}
