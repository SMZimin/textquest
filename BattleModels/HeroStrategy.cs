﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels
{
    public enum HeroStrategy
    {
        MoveToVova = 0,
        MoveToNearestGoodGuy = 1,
        MoveToFinish = 2
    }
}
