﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BattleControl
{
    internal class Drawing
    {
        public static void DrawLine(PictureBox pb, int x1, int y1, int x2, int y2, float borderWidth, Color color)
        {
            pb.Refresh();
            Point p1 = new Point(x1, y1);
            Point p2 = new Point(x2, y2);
            Bitmap map = (Bitmap)pb.Image;
            Graphics g = Graphics.FromImage(map);
            Pen p = new Pen(color, borderWidth);
            g.DrawLine(p, p1, p2);
            pb.Image = map;
            p.Dispose();
            g.Dispose();
        }

        public static void FillRectangle(PictureBox pb, int x1, int y1, int x2, int y2, Color color)
        {
            pb.Refresh();
            Bitmap map = (Bitmap)pb.Image;
            Graphics g = Graphics.FromImage(map);
            Brush brush = new SolidBrush(color);
            g.FillRectangle(brush, x1, y1, x2 - x1, y2 - y1);
            pb.Image = map;
            brush.Dispose();
            g.Dispose();
        }

        public static void FillCircle(PictureBox pb, int centerX, int centerY, int radius, Color color)
        {
            pb.Refresh();
            Bitmap map = (Bitmap)pb.Image;
            Graphics g = Graphics.FromImage(map);
            Brush brush = new SolidBrush(color);
            g.FillEllipse(brush, centerX - radius, centerY - radius, radius * 2, radius * 2);
            pb.Image = map;
            brush.Dispose();
            g.Dispose();
        }
    }
}
