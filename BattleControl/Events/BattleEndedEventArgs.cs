﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleControl.Events
{
    public class BattleEndedEventArgs : EventArgs
    {
        public bool Win { get; }

        public BattleEndedEventArgs(bool win)
        {
            Win = win;
        }
    }
}
