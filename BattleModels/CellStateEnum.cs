﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels
{
    public enum CellStateEnum
    {
        Empty = 0,
        Wall = 1,
        Vova = 2,
        Guard = 3,
        Finish = 4,
        Serega = 5,
        Masha = 6,
        Sonya = 7,
    }
}
