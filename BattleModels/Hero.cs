﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels
{
    public class Hero
    {
        public HeroEnum HeroType { get; }
        public int Hp { get; set; }
        public int Grenades { get; set; }
        public int Armor { get; set; }
        public int MedKits { get; set; }
        public int Accuracy { get; set; }
        public Cell Cell { get; set; }
        public HeroStrategy HeroStrategy { get; set; }
        public int Aggression { get; set; }
        public int Damage { get; set; }

        public Hero(HeroEnum heroType, int hp, int armor, int grenades, int medKits, int accuracy, Cell cell, HeroStrategy heroStrategy, int aggression, int damage)
        {
            HeroType = heroType;
            Hp = hp;
            Armor = armor;
            Grenades = grenades;
            MedKits = medKits;
            Accuracy = accuracy;
            Cell = cell;
            HeroStrategy = heroStrategy;
            Aggression = aggression;
            Damage = damage;
        }
    }
}
