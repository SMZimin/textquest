﻿namespace Grab_the_Cash_
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pbxPicture = new System.Windows.Forms.PictureBox();
            this.rtbMain = new System.Windows.Forms.RichTextBox();
            this.lbxChoice = new System.Windows.Forms.ListBox();
            this.rtbInvent = new System.Windows.Forms.RichTextBox();
            this.btnStartBattle = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxPicture
            // 
            this.pbxPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxPicture.Location = new System.Drawing.Point(960, 66);
            this.pbxPicture.Name = "pbxPicture";
            this.pbxPicture.Size = new System.Drawing.Size(887, 543);
            this.pbxPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxPicture.TabIndex = 0;
            this.pbxPicture.TabStop = false;
            // 
            // rtbMain
            // 
            this.rtbMain.BackColor = System.Drawing.Color.DarkSlateGray;
            this.rtbMain.Font = new System.Drawing.Font("Corbel Light", 18F, System.Drawing.FontStyle.Italic);
            this.rtbMain.ForeColor = System.Drawing.SystemColors.Window;
            this.rtbMain.Location = new System.Drawing.Point(73, 66);
            this.rtbMain.Name = "rtbMain";
            this.rtbMain.ReadOnly = true;
            this.rtbMain.Size = new System.Drawing.Size(827, 543);
            this.rtbMain.TabIndex = 5;
            this.rtbMain.Text = "";
            // 
            // lbxChoice
            // 
            this.lbxChoice.BackColor = System.Drawing.SystemColors.Menu;
            this.lbxChoice.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lbxChoice.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbxChoice.FormattingEnabled = true;
            this.lbxChoice.ItemHeight = 23;
            this.lbxChoice.Location = new System.Drawing.Point(73, 671);
            this.lbxChoice.Name = "lbxChoice";
            this.lbxChoice.Size = new System.Drawing.Size(827, 349);
            this.lbxChoice.TabIndex = 2;
            this.lbxChoice.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbxChoice_DrawItem_1);
            this.lbxChoice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbxChoice_KeyDown);
            // 
            // rtbInvent
            // 
            this.rtbInvent.Font = new System.Drawing.Font("Corbel", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbInvent.Location = new System.Drawing.Point(1573, 671);
            this.rtbInvent.Name = "rtbInvent";
            this.rtbInvent.Size = new System.Drawing.Size(274, 349);
            this.rtbInvent.TabIndex = 3;
            this.rtbInvent.Text = "";
            // 
            // btnStartBattle
            // 
            this.btnStartBattle.Location = new System.Drawing.Point(960, 668);
            this.btnStartBattle.Name = "btnStartBattle";
            this.btnStartBattle.Size = new System.Drawing.Size(75, 23);
            this.btnStartBattle.TabIndex = 6;
            this.btnStartBattle.Text = "Начать бой";
            this.btnStartBattle.UseVisualStyleBackColor = true;
            this.btnStartBattle.Click += new System.EventHandler(this.btnStartBattle_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.btnStartBattle);
            this.Controls.Add(this.rtbInvent);
            this.Controls.Add(this.lbxChoice);
            this.Controls.Add(this.rtbMain);
            this.Controls.Add(this.pbxPicture);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "Grab the Cash!";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxPicture;
        public System.Windows.Forms.RichTextBox rtbMain;
        public System.Windows.Forms.ListBox lbxChoice;
        public System.Windows.Forms.Button btnStartBattle;
        public System.Windows.Forms.RichTextBox rtbInvent;
    }
}

