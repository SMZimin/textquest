﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BattleControl
{
    public partial class FormInfoDialog : Form
    {
        private readonly string _inputText;

        public FormInfoDialog(string text)
        {
            InitializeComponent();
            _inputText = text;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormInfoDialog_Load(object sender, EventArgs e)
        {
            rtbText.AppendText(_inputText + "\r\n");
        }
    }
}
