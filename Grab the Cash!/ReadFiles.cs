﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grab_the_Cash_
{
    class ReadFiles
    {
        public void read_main(string cur_state)
        {
            using (FileStream fstream = File.OpenRead("..\\Main\\" + cur_state + ".txt"))
            {

                // преобразуем строку в байты
                byte[] array = new byte[fstream.Length];
                // считываем данные
                fstream.Read(array, 0, array.Length);
                // декодируем байты в строку
                string textFromFile = Encoding.UTF8.GetString(array);

                (Application.OpenForms[0] as Form1).rtbMain.AppendText(textFromFile);
            }
        }
        public void read_choice(string cur_state)
        {
            if (cur_state == "30")
            {
                //Создание объекта для генерации чисел
                Random rnd = new Random();
                List<int> rands = new List<int>();

                for (int i = 0; i < 13; i++)
                {
                    int value = rnd.Next(1000000, 9999999);
                    rands.Add(value);
                    //Console.WriteLine("rands =" + value);
                }
                rands.Add(3620618);
                List<int> rand_place = new List<int>();
                while (rand_place.Count() != 14)
                {
                    int value = rnd.Next(0, 14);
                    if (!rand_place.Contains(value))
                    {
                        rand_place.Add(value);
                        //Console.WriteLine("rand_place =" + value);
                    }
                }

                (Application.OpenForms[0] as Form1).lbxChoice.Items.Add("1) Вернуться и посмотреть записку");

                for (int i = 0; i < 14; i++)
                {
                    (Application.OpenForms[0] as Form1).lbxChoice.Items.Add((i+2).ToString()+") "+rands[rand_place[i]]);
                }
            }
            else
            {
                StreamReader sr = new StreamReader("..\\Choice\\" + cur_state + ".txt");
                string line;
                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine();
                    if (!(Application.OpenForms[0] as Form1).tupikOnce.Contains(cur_state + line[0].ToString()))
                        (Application.OpenForms[0] as Form1).lbxChoice.Items.Add(line);
                }
                sr.Close();
            }          
        }
        public string read_path(string cur_state, char choice)
        {
            string new_state = "";
            StreamReader sr = new StreamReader("matrix.txt");
            string line = "";
            if (cur_state == "34")
            {
                (Application.OpenForms[0] as Form1).btnStartBattle.PerformClick();
                return "34";
            }
               
            while (!sr.EndOfStream)
            {
                line = sr.ReadLine();
                string path = cur_state + choice;
                int fl = 0;
                for (int i = 0; i < path.Length; i++)
                {
                    if (line[i] == path[i])
                        fl++;

                }
                if (fl == path.Length)
                {
                    new_state = line.Substring(fl + 1);
                    break;
                }
            }
            if (line == "102-11")
            {
                (Application.OpenForms[0] as Form1).tupikOnce.Add("102");
                (Application.OpenForms[0] as Form1).Armor = true;
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("БронеGilette\n");
            }

            if (line == "41-5")
            {
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("Цветы\n");
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("Шампанское\n");
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("Гандоны\n");

            }
            if (line == "51-4")
            {
                (Application.OpenForms[0] as Form1).rtbInvent.Clear();
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("У вас есть:\n");
            }
            if (line == "42-6")
            {
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("Flash-ки\n");
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("ПП Богатырь 9х58\n");
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("Очень много  патронов\n");
            }
            if (line == "141-15")
            {
                (Application.OpenForms[0] as Form1).tupikOnce.Add("141");
                (Application.OpenForms[0] as Form1).Aim = true;
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("ACOG CHPOK 1-1x\n");
            }
                
            if (line == "212-23")
            {
                (Application.OpenForms[0] as Form1).tupikOnce.Add("212");
                (Application.OpenForms[0] as Form1).medkit = 1;
                (Application.OpenForms[0] as Form1).rtbInvent.AppendText("Аптечка Halewa\n");
            }
                
            if (line == "132-16")
            {
                (Application.OpenForms[0] as Form1).AmIGalya = true;
                (Application.OpenForms[0] as Form1).tupikOnce.Add("132");
            }
                
            if (new_state == "9" && (Application.OpenForms[0] as Form1).AmIGalya == true)
                new_state = "17";
            
            sr.Close();

            return new_state;
        }
    }
}
