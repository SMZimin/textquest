﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels
{
    public static class ConfigParams
    {
        public static readonly int MedKitEffect = 75; // Количество восстанавливаемого аптечками здоровья
        public static readonly int GrenadeEffect = 3; // Количество ходов, на которое охранников станит граната

        public static readonly int GrenadesVova = 6; // Стартовое количество гранат у Вовы
        public static readonly int DamageVova = 100; // Стартовый урон Вовы
        public static readonly int AccuracyBasicVova = 50; // Стартовая точность Вовы. Процентная вероятность нанести урон.
        public static readonly int AccuracyWithAimVova = 80; // Точность Вовы с прицелом
        public static readonly int HpVova = 100; // Здоровье Вовы
        public static readonly int ArmorBasicVova = 30; // Стартовая броня Вовы
        public static readonly int ArmorAdvancedVova = 100; // Броня Вовы с бронежилетом
        public static readonly int MedKitsBasicVova = 0; // Стартовое количество аптечек Вовы
        public static readonly int MedKitsAdvancedVova = 1; // Количество аптечек Вовы, как найдет их

        public static readonly int GrenadesSerega = 0; 
        public static readonly int DamageSerega = 100;
        public static readonly int AccuracySerega = 90; // Самый меткий ебать
        public static readonly int HpSerega = 100; 
        public static readonly int ArmorSerega = 30; 
        public static readonly int MedKitsSerega = 0; 
        public static readonly int AggressionSerega = 70; // Агрессия Серёги. Процентная вероятность с которой он будет стрелять, а не бежать при возможности.

        public static readonly int GrenadesMasha = 0;
        public static readonly int DamageMasha = 100;
        public static readonly int AccuracyMasha = 30;
        public static readonly int HpMasha = 200;
        public static readonly int ArmorMasha = 50;
        public static readonly int MedKitsMasha = 0;
        public static readonly int AggressionMasha = 30; // Быстробегающий малодамажущий танк

        public static readonly int GrenadesSonya = 0;
        public static readonly int DamageSonya = 100;
        public static readonly int AccuracySonya = 30;
        public static readonly int HpSonya = 100;
        public static readonly int ArmorSonya = 200;
        public static readonly int MedKitsSonya = 0;
        public static readonly int AggressionSonya = 70; // Косая (из-за шлема ничего не видит) танкующая машина смерти

        public static readonly int GrenadesGuard = 0;
        public static readonly int DamageBasicGuard = 30; // Минимальный урон. Будет ещё рандомно добавляться +0, +10, +20
        public static readonly int AccuracyGuard = 30;
        public static readonly int HpGuard = 100;
        public static readonly int ArmorGuard = 0;
        public static readonly int MedKitsGuard = 0;
        public static readonly int AggressionGuard = 40;
    }
}
