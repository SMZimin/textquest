﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BattleModels
{
    public class Map
    {
        private List<List<Cell>> Cells { get; }

        public int SizeX => Cells.Count;
        public int SizeY => Cells[0].Count;

        public Cell this[int x, int y]
        {
            get => (x >= 0 && x < SizeX && y >=0 && y < SizeY) ? Cells[x][y] : null;
            set => Cells[x][y] = value;
        }

        public Map(int sizeX, int sizeY)
        {
            Cells = new List<List<Cell>>(sizeX);
            for (int i = 0; i < sizeX; i++)
            {
                Cells.Add(new List<Cell>(sizeY));
                for (int j = 0; j < sizeX; j++)
                    Cells[i].Add(new Cell(new CellPosition(i, j), CellStateEnum.Empty));
            }
        }

        public Dictionary<DirectionEnum, Cell> GetCellNeighbors(Cell cell)
        {
            var neighbors = new Dictionary<DirectionEnum, Cell>();
            Cell upCell = this[cell.Position.X, cell.Position.Y - 1];
            if (upCell != null && (upCell.State == CellStateEnum.Empty || upCell.State == CellStateEnum.Finish))
                neighbors.Add(DirectionEnum.Up, upCell);

            Cell downCell = this[cell.Position.X, cell.Position.Y + 1];
            if (downCell != null && (downCell.State == CellStateEnum.Empty || downCell.State == CellStateEnum.Finish))
                neighbors.Add(DirectionEnum.Down, downCell);

            Cell rightCell = this[cell.Position.X + 1, cell.Position.Y];
            if (rightCell != null && (rightCell.State == CellStateEnum.Empty || rightCell.State == CellStateEnum.Finish))
                neighbors.Add(DirectionEnum.Right, rightCell);

            Cell leftCell = this[cell.Position.X - 1, cell.Position.Y];
            if (leftCell != null && (leftCell.State == CellStateEnum.Empty || leftCell.State == CellStateEnum.Finish))
                neighbors.Add(DirectionEnum.Left, leftCell);

            return neighbors;
        }

        public void MoveHero(Cell source, Cell destination)
        {
            var tempState = destination.State;
            destination.State = source.State;
            source.State = tempState;
        }

        public Cell GetCellByPosition(CellPosition position)
        {
            Cell cell = null;
            for (int i = 0; i < SizeX && cell == null; i++)
                for (int j = 0; j < SizeY && cell == null; j++)
                    if (this[i, j].Position == position)
                        cell = this[i, j];

            return cell;
        }

        /// <summary>Разметить все расстояния до любой клетки через алгоритм поиска в ширину (BFS).</summary>
        /// <param name="hero">Герой, для которого считать дистанции.</param>
        internal void TagDistancesForHero(Hero hero)
        {
            ClearDistances();
            TagDistancesToFloor(hero.Cell);
            TagDistancesToHeroes(hero.Cell);
        }

        /// <summary>Разметить всех людей.</summary>
        private void TagDistancesToHeroes(Cell startCell)
        {
            for (int i = 0; i < SizeX; i++)
            {
                for (int j = 0; j < SizeY; j++)
                {
                    var cell = this[i, j];
                    if (cell != startCell 
                        && cell.State != CellStateEnum.Empty
                        && cell.State != CellStateEnum.Wall
                        && cell.State != CellStateEnum.Finish)
                    {
                        TagHeroDistance(cell);
                    }
                }
            }
        }

        private void TagHeroDistance(Cell currentCell)
        {
            var neighbors = GetCellNeighbors(currentCell)
                .Select(p => p.Value)
                .Where(c => c.Distance.HasValue)
                .OrderBy(c => c.Distance)
                .ToList();
            if (neighbors.Count > 0)
            {
                currentCell.Distance = neighbors.First().Distance + 1;
            }
        }

        /// <summary>Разметить все клетки, по которым можно ходить.</summary>
        private void TagDistancesToFloor(Cell startCell)
        {
            List<Cell> initialNeighbors = GetCellNeighbors(startCell).Select(p => p.Value).ToList();
            foreach (var neighbor in initialNeighbors)
            {
                neighbor.Distance = 1;
            }
            Queue<Cell> queue = new Queue<Cell>(initialNeighbors);
            while (queue.Count > 0)
            {
                Cell cell = queue.Dequeue();
                foreach (var neighbor in GetCellNeighbors(cell).Select(p => p.Value).ToList())
                {
                    if (!neighbor.Distance.HasValue)
                    {
                        neighbor.Distance = cell.Distance + 1;
                        queue.Enqueue(neighbor);
                    }
                }
            }
        }

        private void ClearDistances()
        {
            for (int i = 0; i < SizeX; i++)
                for (int j = 0; j < SizeY; j++)
                    this[i, j].Distance = null;
        }

        internal List<Cell> GetRouteToDestinationByCurrentTags(Cell destination)
        {
            var route = new List<Cell>();
            if (destination.Distance.HasValue)
            {
                route.Add(destination);
                for (int curDistance = destination.Distance.Value - 1; curDistance > 0; curDistance--)
                {
                    Cell nextCell = GetCellNeighbors(route.Last())
                        .Select(p => p.Value)
                        .First(c => c.Distance == curDistance);
                    route.Add(nextCell);
                }
                route = route.OrderBy(c => c.Distance).ToList();
            }

            return route;
        }

        internal List<Cell> GetAllCellsOfType(CellStateEnum type)
        {
            var cells = new List<Cell>();
            for (int i = 0; i < SizeX; i++)
            {
                for (int j = 0; j < SizeY; j++)
                {
                    if (this[i, j].State == type)
                        cells.Add(this[i, j]);
                }
            }

            return cells;
        }

        /// <summary>Находятся ли cell1 и cell2 в прямой видимости друг у друга?</summary>
        internal bool IsSeen(Cell cell1, Cell cell2)
        {
            List<Cell> cells = GetAllCellsOnLineBresenham(cell1.Position.X, cell1.Position.Y,
                cell2.Position.X, cell2.Position.Y);

            if (cells.Count > 2)
            {
                cells.Remove(cells.First());
                cells.Remove(cells.Last());
            }

            return cells.All(c => c.State == CellStateEnum.Empty);
        }

        private List<Cell> GetAllCellsOnLineBresenham(int x0, int y0, int x1, int y1)
        {
            var cells = new List<Cell>();

            int dx = (x1 > x0) ? (x1 - x0) : (x0 - x1);
            int dy = (y1 > y0) ? (y1 - y0) : (y0 - y1);
            //Направление приращения
            int sx = (x1 >= x0) ? (1) : (-1);
            int sy = (y1 >= y0) ? (1) : (-1);

            if (dy < dx)
            {
                int d = (dy << 1) - dx;
                int d1 = dy << 1;
                int d2 = (dy - dx) << 1;
                cells.Add(this[x0, y0]);
                int x = x0 + sx;
                int y = y0;
                for (int i = 1; i <= dx; i++)
                {
                    if (d > 0)
                    {
                        d += d2;
                        y += sy;
                    }
                    else
                        d += d1;
                    cells.Add(this[x, y]);
                    x += sx;
                }
            }
            else
            {
                int d = (dx << 1) - dy;
                int d1 = dx << 1;
                int d2 = (dx - dy) << 1;
                cells.Add(this[x0, y0]);
                int x = x0;
                int y = y0 + sy;
                for (int i = 1; i <= dy; i++)
                {
                    if (d > 0)
                    {
                        d += d2;
                        x += sx;
                    }
                    else
                        d += d1;
                    cells.Add(this[x, y]);
                    y += sy;
                }
            }

            return cells;
        }

        #region Serialization/Deserialization

        public string ToJson()
        {
            var mapDto = new MapDto()
            {
                SizeX = this.SizeX,
                SizeY = this.SizeY
            };
            mapDto.Cells = new Cell[SizeX, SizeY];
            for (int i = 0; i < SizeX; i++)
            {
                for (int j = 0; j < SizeY; j++)
                {
                    mapDto.Cells[i, j] = Cells[i][j];
                }
            }

            return JsonConvert.SerializeObject(mapDto, Formatting.Indented);
        }

        public static Map FromJson(string json)
        {
            MapDto mapDto = JsonConvert.DeserializeObject<MapDto>(json);
            var map = new Map(mapDto.SizeX, mapDto.SizeY);
            for (int i = 0; i < mapDto.SizeX; i++)
            {
                for (int j = 0; j < mapDto.SizeY; j++)
                {
                    map[i, j] = mapDto.Cells[i, j];
                }
            }

            return map;
        }

        #endregion
    }
}
