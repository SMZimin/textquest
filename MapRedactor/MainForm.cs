﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BattleModels;
using Newtonsoft.Json;

namespace MapRedactor
{
    public partial class MainForm : Form
    {
        private const int CellSizePx = 10;

        private int MapWidthPx => pbxMap.Width;
        private int MapHeightPx => pbxMap.Height;

        private Map Map { get; set; }

        private Dictionary<CellStateEnum, Color> ColorCache { get; } = new Dictionary<CellStateEnum, Color>();

        public MainForm()
        {
            InitializeComponent();
            InitColorCache();
        }

        private void InitColorCache()
        {
            ColorCache.Add(CellStateEnum.Empty, Color.Transparent);
            ColorCache.Add(CellStateEnum.Wall, Color.DeepPink);
            ColorCache.Add(CellStateEnum.Vova, Color.BlueViolet);
            ColorCache.Add(CellStateEnum.Guard, Color.Red);
            ColorCache.Add(CellStateEnum.Finish, Color.Gold);
            ColorCache.Add(CellStateEnum.Serega, Color.Green);
            ColorCache.Add(CellStateEnum.Masha, Color.Aqua);
            ColorCache.Add(CellStateEnum.Sonya, Color.DarkBlue);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            DrawGrid();
            Map = new Map(MapWidthPx / CellSizePx, MapHeightPx / CellSizePx);
            cbxState.Items.AddRange(Enum.GetValues(typeof(CellStateEnum)).Cast<CellStateEnum>().Select(v => v.ToString()).ToArray());
            cbxState.SelectedIndex = 1;
        }

        private void ReloadPicture()
        {
            pbxMap.Load("..\\..\\..\\Grab the Cash!\\bin\\Picture\\" + "karta-banka.png");
            DrawGrid();
            DrawAllStates();
        }

        private void DrawAllStates()
        {
            for (int i = 0; i < Map.SizeX; i++)
            {
                for (int j = 0; j < Map.SizeY; j++)
                    if (Map[i, j].State != CellStateEnum.Empty)
                        DrawCell(Map[i, j]); 
            }
        }

        private void DrawGrid()
        {
            int x = 0, y = 0;

            while (x < MapWidthPx)
            {
                Drawing.DrawLine(pbxMap, x, 0, x, MapHeightPx, 1, Color.Gray);
                x += CellSizePx;
            }

            while (y < MapHeightPx)
            {
                Drawing.DrawLine(pbxMap, 0, y, MapWidthPx, y, 1, Color.Gray);
                y += CellSizePx;
            }
        }

        private void pbxMap_MouseMove(object sender, MouseEventArgs e)
        {
            CellPosition cellIndex = GetCellPosition(e.X, e.Y);
            toolStripStatusLabelX.Text = cellIndex.X.ToString();
            toolStripStatusLabelY.Text = cellIndex.Y.ToString();
        }

        private CellPosition GetCellPosition(int x, int y)
        {
            return new CellPosition(x / CellSizePx, y / CellSizePx);
        }

        private void pbxMap_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = e as MouseEventArgs;
            CellPosition pos = GetCellPosition(me.X, me.Y);
            if (SelectedState == CellStateEnum.Empty)
            {
                Map[pos.X, pos.Y].State = SelectedState;
                ReloadPicture();
            }
            else if (Map[pos.X, pos.Y].State != SelectedState)
            {
                Map[pos.X, pos.Y].State = SelectedState;
                DrawCell(Map[pos.X, pos.Y]);
            }
        }

        private void DrawCell(Cell cell)
        {
            Drawing.FillRectangle(pbxMap, cell.Position.X * CellSizePx, cell.Position.Y * CellSizePx, 
                (cell.Position.X + 1) * CellSizePx, (cell.Position.Y + 1) * CellSizePx, ColorCache[Map[cell.Position.X, cell.Position.Y].State]);
        }

        private CellStateEnum SelectedState
        {
            get
            {
                CellStateEnum.TryParse((cbxState.SelectedItem ?? CellStateEnum.Empty).ToString(), out CellStateEnum state);
                return state;
            }
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.Cancel || saveFileDialog.FileName == null || saveFileDialog.FileName == "")
                return;
            string filename = saveFileDialog.FileName;
            File.WriteAllText(filename, Map.ToJson());
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel || openFileDialog.FileName == null || openFileDialog.FileName == "")
                return;
            string filename = openFileDialog.FileName;
            string json = File.ReadAllText(filename);
            Map = Map.FromJson(json);
            ReloadPicture();
        }
    }
}
