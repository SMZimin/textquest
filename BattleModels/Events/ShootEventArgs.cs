﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleModels.Events
{
    public class ShootEventArgs : EventArgs
    {
        public string Text { get; }
        public bool GameEnded { get; }

        public ShootEventArgs(string text, bool gameEnded = false)
        {
            Text = text;
            GameEnded = gameEnded;
        }
    }
}
