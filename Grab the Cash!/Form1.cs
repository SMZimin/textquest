﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using BattleControl;
using BattleControl.Events;

namespace Grab_the_Cash_
{
    public partial class Form1 : Form
    {
        SoundPlayer sp = new SoundPlayer("click.wav");
        string cur_state = "1";
        ReadFiles rf = new ReadFiles();
        public List<string> tupikOnce = new List<string>();
        public bool AmIGalya = false;

        public int medkit = 0;
        public bool Armor = false;
        public bool Aim = false;

        public Form1()
        {
           
            InitializeComponent();
            this.lbxChoice.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbxChoice_DrawItem_1);
            pbxPicture.Load("..\\Picture\\"+cur_state+".jpg");
           rtbInvent.AppendText("У вас есть:\n");
        }

        public void update_after_path()
        {
            rtbMain.Clear();
            lbxChoice.Items.Clear();
            pbxPicture.Load("..\\Picture\\" + cur_state + ".jpg");
            rf.read_main(cur_state);
            rf.read_choice(cur_state);
        }

        private void lbxChoice_DrawItem_1(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0) return;
            //if the item state is selected them change the back color 
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                e = new DrawItemEventArgs(e.Graphics,
                                          e.Font,
                                          e.Bounds,
                                          e.Index,
                                          e.State ^ DrawItemState.Selected,
                                          e.ForeColor,
                                          Color.LightBlue);//Choose the color

            // Draw the background of the ListBox control for each item.
            e.DrawBackground();
            // Draw the current item text
            e.Graphics.DrawString(lbxChoice.Items[e.Index].ToString(), e.Font, Brushes.Black, e.Bounds, StringFormat.GenericDefault);
            // If the ListBox has focus, draw a focus rectangle around the selected item.
            e.DrawFocusRectangle();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            rf.read_main(cur_state);
            rf.read_choice(cur_state);
        }

        private void lbxChoice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                sp.Play();
                if (cur_state == "30")
                {
                    if (lbxChoice.SelectedItem.ToString().Substring(lbxChoice.SelectedItem.ToString().Length - 7) ==  "3620618")
                    {
                        cur_state = "31";
                        update_after_path();
                    }
                    else
                    {
                        if (lbxChoice.SelectedItem.ToString() == "1) Вернуться и посмотреть записку")
                        {
                            cur_state = "29";
                            update_after_path();
                        }
                        else
                        {
                            MessageBox.Show("Неверный пароль!");
                            cur_state = "30";
                            update_after_path();
                        }                       
                    }
                }
                else
                {
                    char first = lbxChoice.SelectedItem.ToString()[0];
                    cur_state = rf.read_path(cur_state, first);
                    update_after_path();
                }
                
            }
        }

        #region Battle

        private void btnStartBattle_Click(object sender, EventArgs e)
        {
            StartBattle(medkit, Armor, Aim);
        }

        /// <summary>Запуск игры.</summary>
        /// <param name="medicineChestsCount">Количество аптечек.</param>
        /// <param name="hasArmor">Наличие бронежилета.</param>
        /// <param name="hasAim">Наличие прицела.</param>
        private void StartBattle(int medicineChestsCount, bool hasArmor, bool hasAim)
        {
            var battleControl = new BattleGameControl(medicineChestsCount, hasArmor, hasAim)
            {
                Location = new Point(0, 0),
            };
            battleControl.BattleEnded += OnBattleEnded;
            Controls.Add(battleControl);
            battleControl.BringToFront();
        }

        private void OnBattleEnded(object sender, BattleEndedEventArgs e)
        {
            var battleControl = (BattleGameControl)sender;
            battleControl.BattleEnded -= OnBattleEnded;
            battleControl.SendToBack();
            battleControl.Visible = false;
            Controls.Remove(battleControl);
            HandleBattleEnd(e.Win);
        }

        /// <summary>Обработчик конца боя.</summary>
        /// <param name="win">Выигран ли был бой.</param>
        private void HandleBattleEnd(bool win)
        {
            if (win)
            {
                cur_state = "35";
                update_after_path();
            }
            else
            {
                MessageBox.Show("Вы погибли смертью храбрых. Но мы дадим вам еще один шанс!");
                cur_state = "34";
                update_after_path();
            }
        }

        #endregion
    }
}
