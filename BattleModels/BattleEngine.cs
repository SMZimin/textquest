﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleModels.Events;

namespace BattleModels
{
    public class BattleEngine
    {
        private Map Map { get; }
        private List<Hero> Heroes { get; }
        private Hero Vova => Heroes?.FirstOrDefault(h => h.HeroType == HeroEnum.Vova);
        private int Stun { get; set; } = 0;

        public event EventHandler<StateChangedEventArgs> StateChanged; 
        public event EventHandler<MapChangedEventArgs> MapChanged;
        public event EventHandler<ShootEventArgs> Shooted; 

        #region State
        public int Hp
        {
            get => Heroes?.FirstOrDefault(h => h.HeroType == HeroEnum.Vova)?.Hp ?? 0;
            set
            {
                if (Vova != null)
                {
                    Vova.Hp = value;
                    StateChanged?.Invoke(this, new StateChangedEventArgs());
                }
            }
        }

        public int Grenades
        {
            get => Heroes?.FirstOrDefault(h => h.HeroType == HeroEnum.Vova)?.Grenades ?? 0;
            set
            {
                if (Vova != null)
                {
                    Vova.Grenades = value;
                    StateChanged?.Invoke(this, new StateChangedEventArgs());
                }
            }
        }

        public int Armor
        {
            get => Heroes?.FirstOrDefault(h => h.HeroType == HeroEnum.Vova)?.Armor ?? 0;
            set
            {
                if (Vova != null)
                {
                    Vova.Armor = value;
                    StateChanged?.Invoke(this, new StateChangedEventArgs());
                }
            }
        }

        public int MedKits
        {
            get => Heroes?.FirstOrDefault(h => h.HeroType == HeroEnum.Vova)?.MedKits ?? 0;
            set
            {
                if (Vova != null)
                {
                    Vova.MedKits = value;
                    StateChanged?.Invoke(this, new StateChangedEventArgs());
                }
            }
        }

        public int Accuracy
        {
            get => Heroes?.FirstOrDefault(h => h.HeroType == HeroEnum.Vova)?.Accuracy ?? 0;
            set
            {
                if (Vova != null)
                {
                    Vova.Accuracy = value;
                    StateChanged?.Invoke(this, new StateChangedEventArgs());
                }
            }
        }
        #endregion

        public List<DirectionEnum> AvailableMoves
        {
            get
            {
                var result = new List<DirectionEnum>();
                if (Vova != null)
                {
                    result = Map.GetCellNeighbors(Vova.Cell)
                        .Select(p => p.Key)
                        .ToList();
                }

                return result;
            }
        }
        public int SeenEnemiesCount { get; set; } = 0;

        public BattleEngine(Map map, int armor, int medKits, int accuracy)
        {
            Map = map;
            Heroes = HeroInitializer.Init(map);
            Armor = armor;
            MedKits = medKits;
            Accuracy = accuracy;
        }

        public void UpdateSeenEnemiesCount()
        {
            Map.TagDistancesForHero(Vova);
            SeenEnemiesCount = GetSeenHeroesOfType(Vova.Cell, HeroEnum.Guard).Count;
        }

        public void Shoot(int enemyIndex)
        {
            Map.TagDistancesForHero(Vova);
            var enemy = GetSeenHeroesOfType(Vova.Cell, HeroEnum.Guard)[enemyIndex];
            bool hit = new Random().Next(101) < Vova.Accuracy;
            if (hit)
            {
                enemy.Cell.State = CellStateEnum.Empty;
                Heroes.Remove(enemy);
                string msg = "Вы с огромным удовольствием накормили бегущего на вас бойца свинцом. Он мёртв.";
                Shooted?.Invoke(this, new ShootEventArgs(msg));
            }
            else
            {
                string msg = "Вы дали беглую очередь по врагу, но пули не достигли цели.";
                Shooted?.Invoke(this, new ShootEventArgs(msg));
            }
        }

        public CellStateEnum Move(DirectionEnum direction)
        {
            CellStateEnum newState = CellStateEnum.Vova;

            CellPosition destPos;
            if (direction == DirectionEnum.Left)
                destPos = new CellPosition(Vova.Cell.Position.X - 1, Vova.Cell.Position.Y);
            else if (direction == DirectionEnum.Right)
                destPos = new CellPosition(Vova.Cell.Position.X + 1, Vova.Cell.Position.Y);
            else if (direction == DirectionEnum.Up)
                destPos = new CellPosition(Vova.Cell.Position.X, Vova.Cell.Position.Y - 1);
            else 
                destPos = new CellPosition(Vova.Cell.Position.X, Vova.Cell.Position.Y + 1);

            Cell cell = Map.GetCellByPosition(destPos);
            if (cell.State == CellStateEnum.Finish)
            {
                newState = CellStateEnum.Finish;
            }
            else
            {
                Map.MoveHero(Vova.Cell, cell);
                Vova.Cell = cell;
                //MapChanged?.Invoke(this, new MapChangedEventArgs());
            }

            return newState;
        }

        public void ThrowGrenade()
        {
            Stun += ConfigParams.GrenadeEffect;
            Grenades--;
        }

        public void DoOtherHeroesTurn()
        {
            foreach (var hero in Heroes)
            {
                if (hero.HeroType == HeroEnum.Serega || hero.HeroType == HeroEnum.Masha || hero.HeroType == HeroEnum.Sonya)
                    DoHeroTurn(hero);
                else if (hero.HeroType == HeroEnum.Guard && Stun == 0 && hero.Cell.State != CellStateEnum.Empty)
                {
                    DoHeroTurn(hero);
                }
            }

            TryRemoveExitedHeroes();
            if (Stun != 0)
                Stun--;

            StateChanged?.Invoke(this, new StateChangedEventArgs());
            MapChanged?.Invoke(this, new MapChangedEventArgs());
        }

        public void ChangeStrategy(HeroEnum heroes, HeroStrategy newStrategy)
        {
            foreach (var hero in Heroes.Where(h => h.HeroType == heroes).ToList())
            {
                hero.HeroStrategy = newStrategy;
            }
        }

        public bool IsPresent(HeroEnum hero)
        {
            return Heroes.Any(h => h.HeroType == hero);
        }

        private void TryRemoveExitedHeroes()
        {
            var exitedHeroes = Heroes.Where(h => h.Cell.State == CellStateEnum.Empty).ToList();
            foreach (var hero in exitedHeroes)
            {
                Heroes.Remove(hero);
            }
        }

        private void DoHeroTurn(Hero hero)
        {
            Map.TagDistancesForHero(hero);
            var seenEnemies = new List<Hero>();
            if (hero.HeroType != HeroEnum.Guard)
            {
                seenEnemies.AddRange(GetSeenHeroesOfType(hero.Cell, HeroEnum.Guard));
            }
            else
            {
                seenEnemies.AddRange(GetSeenHeroesOfType(hero.Cell, HeroEnum.Vova));
                seenEnemies.AddRange(GetSeenHeroesOfType(hero.Cell, HeroEnum.Serega));
                seenEnemies.AddRange(GetSeenHeroesOfType(hero.Cell, HeroEnum.Masha));
                seenEnemies.AddRange(GetSeenHeroesOfType(hero.Cell, HeroEnum.Sonya));
            }
            seenEnemies = seenEnemies.OrderBy(h => h.Cell.Distance).ToList();

            if (seenEnemies.Count > 0 && ChooseTurnSolution(hero.Aggression) == TurnSolutionEnum.Shoot)
            {
                Hero enemy = seenEnemies.First();
                bool hit = new Random().Next(101) < hero.Accuracy;
                if (hit)
                {
                    if (enemy.HeroType == HeroEnum.Guard)
                    {
                        enemy.Cell.State = CellStateEnum.Empty;
                        string mes = null;
                        if (hero.HeroType == HeroEnum.Serega)
                            mes = $"Серчик мастерски шлёт пулю в лоб противнику! И ведь он только разогревается!";
                        else if (hero.HeroType == HeroEnum.Masha)
                            mes = $"Мавыка даёт очередь из автомата и насмерть сражает своей красотой очередного врага!";
                        else if (hero.HeroType == HeroEnum.Sonya)
                            mes = $"Шлем постоянно съезжает на глаза Фофарику, но она упорно палит во все стороны из всех стволов и убивает ещё одного врага!";
                        Shooted?.Invoke(this, new ShootEventArgs(mes));
                    }
                    else
                    {
                        if (enemy.Armor > 0)
                        {
                            enemy.Armor -= hero.Damage;
                            if (enemy.Armor < 0)
                                enemy.Armor = 0;
                            string mes = null;
                            if (enemy.HeroType == HeroEnum.Vova)
                                mes = $"Вас задело автоматной очередью!";
                            else if (enemy.HeroType == HeroEnum.Serega)
                                mes = $"Серчик словил маслину!\r\nЕго броня теперь - {enemy.Armor}, " +
                                      $"а здоровье - {enemy.Hp}.";
                            else if (enemy.HeroType == HeroEnum.Masha)
                                mes = $"Мавыку ранили!\r\nЕё броня теперь - {enemy.Armor}, " +
                                      $"а здоровье - {enemy.Hp}.";
                            else if (enemy.HeroType == HeroEnum.Sonya)
                                mes = $"В Фофарика попали!\r\nЕё броня теперь - {enemy.Armor}, " +
                                      $"а здоровье - {enemy.Hp}.";
                            Shooted?.Invoke(this, new ShootEventArgs(mes));
                        }
                        else
                        {
                            enemy.Hp -= hero.Damage;
                            string mes = null;
                            if (enemy.Hp > 0)
                            {
                                if (enemy.HeroType == HeroEnum.Vova)
                                    mes = $"Вас задело автоматной очередью!";
                                else if (enemy.HeroType == HeroEnum.Serega)
                                    mes = $"Серчик словил маслину!\r\nЕго броня теперь - {enemy.Armor}, " + 
                                          $"а здоровье - {enemy.Hp}.";
                                else if (enemy.HeroType == HeroEnum.Masha)
                                    mes = $"Мавыку ранили!\r\nЕё броня теперь - {enemy.Armor}, " +
                                          $"а здоровье - {enemy.Hp}.";
                                else if (enemy.HeroType == HeroEnum.Sonya)
                                    mes = $"В Фофарика попали!\r\nЕё броня теперь - {enemy.Armor}, " +
                                          $"а здоровье - {enemy.Hp}.";
                                Shooted?.Invoke(this, new ShootEventArgs(mes));
                            }
                            else
                            {
                                if (enemy.HeroType == HeroEnum.Vova)
                                    mes = $"Вы бились храбро, но не на соточку. Итог - вас убили. Попробуйте ещё раз!";
                                else if (enemy.HeroType == HeroEnum.Serega)
                                    mes = $"Вы дрались как львы, но в пылу боя не заметили, как Серчика взяли числом и убили! " +
                                          $"Невосполнимая потеря для всех нас! В следующий раз воюйте лучше!";
                                else if (enemy.HeroType == HeroEnum.Masha)
                                    mes = $"Мавыка билась бесстрашно, но и её догнала пуля. Без такого бойца сопротивление бесполезно. " +
                                          $"В следующий раз воюйте лучше!";
                                else if (enemy.HeroType == HeroEnum.Sonya)
                                    mes = $"Они убили Фофарика! Сволочи!\r\nХоть Фофарик и был настоящей машиной смерти, но и она не бессмертна. " +
                                          $"В следующий раз воюйте лучше!";
                                Shooted?.Invoke(this, new ShootEventArgs(mes, true));
                            }
                        }
                    }
                }
                else
                {
                    if (enemy.HeroType == HeroEnum.Guard)
                    {
                        string mes = null;
                        if (hero.HeroType == HeroEnum.Serega)
                            mes = $"Серчик довыёбывался и промазал!";
                        else if (hero.HeroType == HeroEnum.Masha)
                            mes = $"Мавыка поймала на мушку врага, нажала на спусковой крючок, но поняла, что забыла перезарядиться!";
                        else if (hero.HeroType == HeroEnum.Sonya)
                            mes = $"Шлем съехал на глаза Фофарику и она ни в кого не попала! Хоть и отстреляла целую обойму.";
                        Shooted?.Invoke(this, new ShootEventArgs(mes));
                    }
                    else
                    {
                        string mes = null;
                        if (enemy.HeroType == HeroEnum.Vova)
                            mes = $"В вас стреляли, но годы изучения каратэ у местных гопников позволили вам увернуться.";
                        else if (enemy.HeroType == HeroEnum.Serega)
                            mes = $"В Серчика выпустили целую обойму, но промазали. Странно, ведь он такой большой.";
                        else if (enemy.HeroType == HeroEnum.Masha)
                            mes = $"Мавыка закрыла глаза, и все выпущенные в неё пули чудесным образом ушли в молоко! Везучая чертовка!";
                        else if (enemy.HeroType == HeroEnum.Sonya)
                            mes = $"Фофарик даже не думал уворачиваться, а грозно шёл на врага. Все пули тупо отрикошетили от её огромной каски! " +
                                  $"Её точно стоит бояться.";
                        Shooted?.Invoke(this, new ShootEventArgs(mes));
                    }
                }
            }
            else
            {
                MoveNextCell(hero);
            }
        }

        private List<Hero> GetSeenHeroesOfType(Cell pov, HeroEnum heroType)
        {
            var seenHeroes = new List<Hero>();
            var heroes = Heroes.Where(h => h.Cell.Distance.HasValue && h.HeroType == heroType).ToList();
            foreach (var hero in heroes)
            {
                if (Map.IsSeen(pov, hero.Cell))
                    seenHeroes.Add(hero);
            }

            return seenHeroes;
        }

        private void MoveNextCell(Hero movingHero)
        {
            if (movingHero.HeroStrategy == HeroStrategy.MoveToVova)
            {
                List<Cell> route = Map.GetRouteToDestinationByCurrentTags(Vova.Cell);
                if (route.Count > 1)
                {
                    Map.MoveHero(movingHero.Cell, route.First());
                    movingHero.Cell = route.First();
                }
            }
            else if (movingHero.HeroStrategy == HeroStrategy.MoveToNearestGoodGuy)
            {
                Hero nearestHero = Heroes.FirstOrDefault(h => h.Cell.Distance.HasValue && h.HeroType != HeroEnum.Guard);
                if (nearestHero != null)
                {
                    foreach (var hero in Heroes)
                    {
                        if (hero.Cell.Distance.HasValue && hero.HeroType != HeroEnum.Guard &&
                            hero.Cell.Distance < nearestHero.Cell.Distance)
                            nearestHero = hero;
                    }

                    List<Cell> route = Map.GetRouteToDestinationByCurrentTags(nearestHero.Cell);
                    if (route.Count > 1)
                    {
                        Map.MoveHero(movingHero.Cell, route.First());
                        movingHero.Cell = route.First();
                    }
                }
            }
            else if (movingHero.HeroStrategy == HeroStrategy.MoveToFinish)
            {
                var finishCell = Map.GetAllCellsOfType(CellStateEnum.Finish)
                    .Where(c => c.Distance.HasValue)
                    .OrderBy(c => c.Distance)
                    .FirstOrDefault();
                if (finishCell != null) 
                {
                    List<Cell> route = Map.GetRouteToDestinationByCurrentTags(finishCell);
                    if (route.Count > 1)
                    {
                        Map.MoveHero(movingHero.Cell, route.First());
                        movingHero.Cell = route.First();
                    }
                    else if (route.Count == 1)
                    {
                        movingHero.Cell.State = CellStateEnum.Empty;
                    }
                }
            }
        }

        private TurnSolutionEnum ChooseTurnSolution(int heroAggression)
        {
            var result = TurnSolutionEnum.Move;
            if (new Random().Next(101) < heroAggression)
                result = TurnSolutionEnum.Shoot;

            return result;
        }
    }
}
